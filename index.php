<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title></title>
    </head>
    <body>
        <?php
        //Возвращает массив со списком текстовых файлов в директории
        function GetFileList($d = __DIR__)
        {
            $arrayFiles = array();
            foreach (glob($d . "\*.txt") as $filename) {
                $arrayFiles[] = $filename;
            }
            return $arrayFiles;
        }
        
        //Возвращает массив с персональными данными пользователя
        function GetTextFileContent($f) 
        {
            $file = file($f);
            $array = array();
            $array['FIO'] = $file[0];
            $array['MAIL'] = $file[1];
            $array['DATE'] = date_create($file[2]);
            $array['ADDRESS'] = RightAddress($file[3]);
            return $array;
        }
        
        //Возвращает корректный адрес персональной страницы
        function RightAddress($str)
        {
            if(substr($str, 0, 7) == 'http://'){
                $str = str_replace("http://", "", $str);
            }
            if(substr($str, 0, 4) == 'www.'){
                $str = str_replace("www.", "", $str);
            }
            return 'http://www.' . $str;
        }
        
        //Возвращает массив, элементы которого - массивы с данными о пользователях, а ключи - e-mail соответствующего юзера
        //Массив отсортирован по дате регистрации пользователя
        function CreateTable()
        {
            $table = array();
            foreach (GetFileList() as $file){
                $table[GetTextFileContent($file)['MAIL']] = GetTextFileContent($file);
            }
            uasort($table, function($x, $y){
                if ($x['DATE'] > $y['DATE']){
                    return 1;
                }
                elseif ($x['DATE'] < $y['DATE']){
                    return -1;
                }
                else{
                    return 0;
                }
                });
            return $table;
        }

        //Функция вывода списка пользователей на страницу
        function ShowTable($table)
        {
            echo "<center><H1>Список всех пользователей:</H1>";
            echo "<table border='black'>
            <tr>
                <th>ФИО</th>
                <th>E-mail</th>
                <th>Дата регистрации</th>
                <th>Web-адрес</th>
            </tr>";
            
            array_map(function($userInfo){
                echo "<tr>";
                array_map(function($value, $key){
                    switch($key){
                        case 'MAIL':
                            echo "<td><a href=mailto:$value>$value</a></td>";
                            break;
                        case 'ADDRESS':
                            echo "<td><a href=$value>$value</a></td>";
                            break;
                        case 'DATE':
                            echo "<td>" . date_format($value, "d.m.Y") . "</td>";
                            break;
                        default:
                            echo "<td>" . htmlentities($value) . "</td>";
                    }
                    }, $userInfo, array_keys($userInfo));
                echo "</tr>";
                }, $table);
                echo "</table></center>";
        }
        
        //Функция вывода списка пользователей с e-mail в зоне .com на страницу
        //Список отсортирован по длине e-mail
        function ShowTableOnlyWithCom($table)
        {
            echo "<center><H1>Список пользователей c E-mail в зоне .com:</H1>";
            echo "<table border='black'>
            <tr>
                <th>ФИО</th>
                <th>E-mail[&#8595]</th>
                <th>Дата регистрации</th>
                <th>Web-адрес</th>
            </tr>";   
            uksort($table, function ($a, $b){return strlen($b)- strlen($a);});
            array_map(function($userInfo){
                echo "<tr>";
                array_map(function($value, $key){
                    switch($key){
                        case 'MAIL':
                            echo "<td><a href=mailto:$value>$value</a></td>";
                            break;
                        case 'ADDRESS':
                            echo "<td><a href=$value>$value</a></td>";
                            break;
                        case 'DATE':
                            echo "<td>" . date_format($value, "d.m.Y") . "</td>";
                            break;
                        default:
                            echo "<td>" . htmlentities($value) . "</td>";
                    }
                }, $userInfo, array_keys($userInfo));
                echo "</tr>";
                }, array_filter($table, function ($row){return (substr($row['MAIL'], -6, 4) == ".com");}));
                echo "</table></center>";
        }


        $table = CreateTable();
        ShowTable($table);
        ShowTableOnlyWithCom($table);
        ?>
    </body>
</html>
